const url = require('url');
const path = require('path');
const fs = require('fs');
const { dialog } = require('electron');

exports.getDirectory = targetWindow => {
	const dir = dialog.showOpenDialog(targetWindow, {
		properties: ['openDirectory']
	});
	if (!dir || !dir.length) return;
	return dir[0];
};

exports.getFiles = directory => new Promise((resolve, reject) => {
	fs.readdir(directory, (err, files) => {
		if (!!err) console.error(err);
		resolve(files);
	});
});

exports.windowPath = (windowId) => {
	return url.format({
    pathname: path.join(__dirname, windowId, 'index.html'),
    protocol: 'file:',
    slashes: true
  });
};
