const { remote, ipcRenderer } = require('electron');
const path = require('path');
const url = require('url');
const $ = require('jquery');

(function() {
	let isFullScreen = false;

  $(window).on('keydown', (e) => {
    if(e.key == 'Escape' || e.key == 'Esc' || e.keyCode == 27) {
      ipcRenderer.send('minimizeDisplay');
			isFullScreen = false;
    }
  });

  $(window).on('keydown', (e) => {
    if(e.key == 'Enter' || e.keyCode == 13) {
      ipcRenderer.send('maximizeDisplay');
			isFullScreen = true;
    }
  });

  $(window).on('dblclick', (e) => {
		if (isFullScreen) {
			ipcRenderer.send('minimizeDisplay');
		} else {
			ipcRenderer.send('maximizeDisplay');
		}
		isFullScreen = !isFullScreen;
  });

  ipcRenderer.on('opacity', (event, value) => {
    v1.style.opacity = value;
  });

  ipcRenderer.on('s1', (event, value) => {
    $("#v1").html(value);
  });
  ipcRenderer.on('s2', (event, value) => {
    $("#v2").html(value);
  });

})();
