const { remote, ipcRenderer } = require('electron');
const url = require('url');
const path = require('path');
let $ = require('jquery');

function reloadDir () {
	if (localStorage) {
		return localStorage.getItem('mediaBGdir');
	} else {
		return null;
	}
}

function saveDir (dir) {
	if (localStorage) {
		localStorage.setItem('mediaBGdir', dir);
	}
}

function setOptions (files) {
	$("#s1").children().remove();
	$("#s2").children().remove();
  $("#s1").prepend(`<option value="-">brak tła</option>`);
  $("#s2").prepend(`<option value="-">brak tła</option>`);
	files.filter(filterFile).forEach(file => {
		const html = `<option value="${file}">${file}</option>`;
	  $("#s1").append(html);
	  $("#s2").append(html);
	});
}

(function() {
  $('#opacity').on('input', () => {
    const opacity = 1 - $('#opacity').val();
    ipcRenderer.send('opacity', opacity);
  });
  changed = (id) => {
    return () => {
      const value = loadFile($('#s' + id).val());
      $("#p" + id).html(value);
      ipcRenderer.send('s' + id, value);
    }
  };

  ipcRenderer.on('directory', (event, value) => {
		$("#directoryName").val(value.directory);
		setOptions(value.files);
		saveDir(value.directory);
  });

	$("#selectDirectory").click(() => ipcRenderer.send('selectDirectory'));

	if (reloadDir() === null) {
		ipcRenderer.send('defaultDirectory');
	} else {
		ipcRenderer.send('loadDirectory', reloadDir());
	}

	$('#s1').on('change', changed(1));
	$('#s2').on('change', changed(2));
	setTimeout(() => {
		$('#s1').val($('#s1').find('option').first().attr('value')).change();
		$('#s2').val($('#s2').find('option').first().attr('value')).change();
	}, 1000);
})();

function loadImage (filePath) {
  return `<div class="img" style="background-image: url(${filePath})"></div>`;
}

function loadVideo (filePath, type) {
  const v = `
    <video muted plays-inline loop autoplay>
      <source src="${filePath}" type="${type}">
    </video>
  `;
  return v;
}

const allowedFiles = {
	image: [
		'jpg',
		'jpeg',
		'png',
		'bmp',
		'svg'
	],
	'video': {
		'webm': 'video/webm',
		'mp4': 'video/mp4',
		'mov': 'video/mp4',
		'ogv': 'video/ogg'
	}
};

function filterFile (name) {
	const ext = name.split('.').pop();
	const isExt = allowed => allowed === ext;
	if (allowedFiles.image.find(isExt) !== undefined) return 'image';
	if (Object.keys(allowedFiles.video).find(isExt) !== undefined) return 'video';
	return false;
}

window.loadFile = (filePath) => {
	if (!filePath || filePath === '-') {
		return `<div class="img" style="background: black !important"></div>`;
	}
  const loadPath = url.format({
    pathname: path.join($("#directoryName").val(), filePath),
    protocol: 'file:',
    slashes: true
  }).split('\\').join('/');
  let content = '';
  if (filterFile(filePath) === 'video') {
		const ext = filePath.split('.').pop();
    content = loadVideo(loadPath, allowedFiles.video[ext]);
  } else {
    content = loadImage(loadPath);
  }
  return content;
}
