const express = require('express');
const {app, BrowserWindow, ipcMain} = require('electron');
const path = require('path');
const url = require('url');
const {getDirectory, getFiles, windowPath} = require('./src/helpers.js');

let store = {};

function createWindows () {
	store.panelWindow = new BrowserWindow({
		width: 1200,
		minWidth: 790,
		height: 800,
		icon: path.join(__dirname, 'assets/icons/png/64x64.png')
	});
	store.panelWindow.loadURL(windowPath('panel'));
	store.panelWindow.on('closed', () => {
		store.panelWindow = null;
		if (store.displayWindow) store.displayWindow.close();
	});
	// store.panelWindow.webContents.openDevTools(/*{detach:true}*/);

	setTimeout(() => {
		store.displayWindow = new BrowserWindow({
			width: 600,
			height: 400
		});
		store.displayWindow.setMenu(null);
		store.displayWindow.loadURL(windowPath('display'));
		store.displayWindow.on('closed', () => {
			store.displayWindow = null;
			if (store.panelWindow) store.panelWindow.close();
		});
	  // store.displayWindow.webContents.openDevTools(/*{detach:true}*/);
	}, 300);
}

app.on('ready', createWindows);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (!panelProcess.isOpen(appState, 'panel')) {
    createWindow();
  }
});


// -------------IPC -----------

function sendFileList (dir) {
	getFiles(dir).then((files) => {
		store.panelWindow.webContents.send('directory', {
			directory: dir,
			files: files
		});
	}, () => {});
};

ipcMain.on('selectDirectory', () => sendFileList(getDirectory(store.panelWindow)));
ipcMain.on('defaultDirectory', () => sendFileList(__dirname));
ipcMain.on('loadDirectory', (event, value) => sendFileList(value));

ipcMain.on('maximizeDisplay', () => store.displayWindow.setFullScreen(true));
ipcMain.on('minimizeDisplay', () => store.displayWindow.setFullScreen(false));
ipcMain.on('hide', () => store.displayWindow.hide());
ipcMain.on('show', () => store.displayWindow.show());

// Relay

ipcMain.on('opacity', (event, value) => {
  store.displayWindow.webContents.send('opacity', value);
});

ipcMain.on('s1', (event, value) => {
  store.displayWindow.webContents.send('s1', value);
});
ipcMain.on('s2', (event, value) => {
  store.displayWindow.webContents.send('s2', value);
});
